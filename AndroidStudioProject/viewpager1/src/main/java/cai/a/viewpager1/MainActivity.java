package cai.a.viewpager1;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * 主要分为
 *   一，布局中使用<android.support.v4.view.ViewPager>,必须是全面 否则会报错
 *   二，用数组封装View（可以是ImageView，也可以是layout布局，官方建议和Fragment结合使用）
 *   三，继承ViewPager（实现四个方法 getCount(),destroyItem(),instantiateItem(),isViewFromObject()）
 *
 *   详细教程可以看:http://blog.csdn.net/harvic880925/article/details/38453725
 *
 *
 *
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private ViewPager mViewPager;
    private List<View> listView;
    private View view1;
    private View view2;
    private View view3;
    private View view4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewPager = (ViewPager)findViewById(R.id.viewpager);

        LayoutInflater lf = getLayoutInflater().from(this);
        listView = new ArrayList<View>();

        view1 = lf.inflate(R.layout.view1,null);
        view2 = lf.inflate(R.layout.view2,null);
        view3 = lf.inflate(R.layout.view3,null);
        view4 = lf.inflate(R.layout.view4,null);

        listView.add(view1);
        listView.add(view2);
        listView.add(view3);
        listView.add(view4);


        PagerAdapter adapter = new PagerAdapter() {

            /**返回view个数*/
            @Override
            public int getCount() {
                Log.i(TAG, "getCount");
                return listView.size();
            }

            /**移除ViewGroup里面的View*/
            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                Log.i(TAG, "destoryItem");
                container.removeView(listView.get(position));
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                Log.i(TAG, "instantiateItem");
                container.addView(listView.get(position));
                Log.i(TAG, "instantiateItem" + position + ">>>>>>>>>>>>>>>>>");
                if (listView.size() == position + 1) {
                    Button btn = (Button) container.findViewById(R.id.btn);
                    btn.setVisibility(View.VISIBLE);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(MainActivity.this, "你点击了按钮", Toast.LENGTH_SHORT);
                        }
                    });
                }
                return listView.get(position);
            }

            /**官方推荐这样写*/
            @Override
            public boolean isViewFromObject(View view, Object object) {
                Log.i(TAG, "isViewFromObject");
                return view == object;
            }

        };

        mViewPager.setAdapter(adapter);

    }
}
